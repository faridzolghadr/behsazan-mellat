import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class PanelSidebarService {
    private _isPanelSidebar = new BehaviorSubject<'messages' | 'notification' | 'none'>('none');
    readonly isPanelSidebar$ = this._isPanelSidebar.asObservable();

    setPanelSidebar(value: 'messages' | 'notification' | 'none') {
        this._isPanelSidebar.next(value);
    }
}