import { Component, OnInit } from '@angular/core';
import { PanelSidebarService } from 'src/app/services/panel-sidebar.service';

@Component({
  selector: 'app-header-section',
  templateUrl: './header-section.component.html',
  styleUrls: ['./header-section.component.scss']
})
export class HeaderSectionComponent implements OnInit {

  constructor(private sidebarPanelService: PanelSidebarService) { }

  ngOnInit() {
  }

  onChangeStatePanel(value: 'messages' | 'notification') {
    this.sidebarPanelService.setPanelSidebar(value);
  }
}
