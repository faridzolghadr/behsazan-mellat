import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderSectionComponent } from './header-section.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HeaderSectionComponent, ProfileComponent],
  exports: [HeaderSectionComponent]
})
export class HeaderSectionModule { }
