import { Component, OnInit } from '@angular/core';
import { PanelSidebarService } from 'src/app/services/panel-sidebar.service';
import { MESSAGE_MOCKETT_DATA, NOTIFICATION_MOCKETT_DATA } from './constants';
import { MessageModel } from './model';

@Component({
  selector: 'app-panel-sidebar',
  templateUrl: './panel-sidebar.component.html',
  styleUrls: ['./panel-sidebar.component.scss']
})
export class PanelSidebarComponent implements OnInit {
  tabState = this.sidebarPanelService.isPanelSidebar$;
  dataSource: MessageModel[] = [];
  constructor(private sidebarPanelService: PanelSidebarService) { }

  ngOnInit() {
    this.getDataFromApi()
  }

  onChangeState(value: 'messages' | 'notification' | 'none') {
    this.sidebarPanelService.setPanelSidebar(value);
  }

  private getDataFromApi() {
    this.sidebarPanelService.isPanelSidebar$.subscribe(it => {
      this.dataSource = [];
      if (it === 'none') return;
      this.dataSource = it === 'messages' ? MESSAGE_MOCKETT_DATA : NOTIFICATION_MOCKETT_DATA;
    })
  }
}
