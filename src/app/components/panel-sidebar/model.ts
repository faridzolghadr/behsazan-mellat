export interface MessageModel {
    date: string;
    type: 'M' | 'N';
    fullName: string;
    description: string;
    isRead: boolean;
}