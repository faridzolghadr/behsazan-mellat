import { MessageModel } from "./model"


export const MESSAGE_MOCKETT_DATA: MessageModel[] = [
    {
        date: 'July 3 , 11:01 am',
        type: "M",
        fullName: 'Farid Zolghadr',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        isRead: false
    },
    {
        date: 'July 4 , 12:11 am',
        type: "M",
        fullName: 'Saleh Katebi',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        isRead: false
    },
    {
        date: 'July 4 , 13:21 pm',
        type: "M",
        fullName: 'Ali Mardaneh',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        isRead: false
    }
]

export const NOTIFICATION_MOCKETT_DATA: MessageModel[] = [
    {
        date: 'July 2 , 12:31 pm ',
        type: "N",
        fullName: 'Reza Heydari',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        isRead: false
    },
    {
        date: 'July 2 , 12:58 pm',
        type: "N",
        fullName: 'Ali Noori',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        isRead: false
    },
    {
        date: 'July 3 , 11:01 am',
        type: "N",
        fullName: 'Behrooz Fard',
        description: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
        isRead: false
    }
]