import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelSidebarComponent } from './panel-sidebar.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PanelSidebarComponent],
  exports: [PanelSidebarComponent],
})
export class PanelSidebarModule { }
