import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuSectionComponent } from './menu-section.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MenuSectionComponent],
  exports: [MenuSectionComponent],
})
export class MenuSectionModule { }
