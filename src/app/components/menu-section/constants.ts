import { MenuModel } from "./model";

export const MENU: MenuModel[] = [
    {
        title: 'Dashboard',
        icon: 'dashboard',
        path: '#'
    },
    {
        title: 'users',
        icon: 'group',
        path: '#',
    },
    {
        title: 'Statistics',
        icon: 'pie_chart',
        path: '#',
    },
    {
        title: 'Contacts',
        icon: 'contacts',
        path: '#',
        child: [
            {
                title: 'child 1',
                path: '#',
                child: [
                    {
                        title: 'child 1_1',
                        path: '#',
                    },
                    {
                        title: 'child 1_2',
                        path: '#',
                    }
                ]
            },
            {
                title: 'child 2',
                path: '#',
            },
            {
                title: 'child 3',
                path: '#',
            }
        ]
    },
    {
        title: 'Servers',
        icon: 'dns',
        path: '#',
        child: [
            {
                title: 'child 1',
                path: '#',
            },
            {
                title: 'child 2',
                path: '#',
            },
            {
                title: 'child 3',
                path: '#',
                child: [
                    {
                        title: 'child 1_1',
                        path: '#',
                    },
                    {
                        title: 'child 1_2',
                        path: '#',
                    }
                ]
            }
        ]
    },
    {
        title: 'Settings',
        icon: 'settings',
        path: '#'
    },
]