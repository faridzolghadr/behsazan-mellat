import { Component, OnInit } from '@angular/core';
import { MenuModel } from './model';
import { MENU } from './constants';

@Component({
  selector: 'app-menu-section',
  templateUrl: './menu-section.component.html',
  styleUrls: ['./menu-section.component.scss']
})
export class MenuSectionComponent implements OnInit {
  dataSource: MenuModel[] = MENU;
  isCollapse: boolean = false;
  constructor() { }

  ngOnInit() {
  }

}
