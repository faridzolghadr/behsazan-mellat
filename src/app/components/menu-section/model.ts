export interface MenuModel {
    icon?: string;
    title: string;
    path: string;
    child?: MenuModel[];
}