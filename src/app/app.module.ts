import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderSectionModule } from './components/header-section/header-section.module';
import { MenuSectionModule } from './components/menu-section/menu-section.module';
import { PanelSidebarModule } from './components/panel-sidebar/panel-sidebar.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HeaderSectionModule,
    PanelSidebarModule,
    MenuSectionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
